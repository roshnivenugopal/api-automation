package com.ad2pro.spectra.automation.resource;

import com.ad2pro.spectra.automation.utils.InitializeTestData;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.when;

public class GetCall {

    private static final Logger logger = Logger.getLogger("GetCall.class");

    DateTimeFormatter datetimeformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
    LocalDateTime timenow = LocalDateTime.now();
    String date = datetimeformatter.format(timenow);


    @Test(dataProviderClass = InitializeTestData.class, dataProvider = "readFromFile",  invocationCount = 6, threadPoolSize = 3)
    public void captureResponseTime(String get, String baseUrl, String port, String routePath,  String params, String response, String code) throws IOException {

        //initialising the base uri,
        RestAssured.baseURI = baseUrl;
        RestAssured.port = Integer.parseInt(port);

        Response finalResponse = getResponseWithRouterPathAndCode(code, routePath);

        logger.info("Response Time Took: "  + finalResponse.getTimeIn(TimeUnit.MILLISECONDS) + " and Thread: "+Thread.currentThread().getName());
        logger.info(finalResponse.prettyPrint());
        Assert.assertEquals(finalResponse.getStatusCode(), Integer.parseInt(code));
    }

    @Test(dataProviderClass = InitializeTestData.class, dataProvider = "readFromFile")
    public void captureResponseIntoCSV(String get, String baseUrl, String port, String routePath,  String params, String response, String code) throws IOException, ParseException {
        String header = "";
        String body = "";

        //initialising the base uri,
        RestAssured.baseURI = baseUrl;
        RestAssured.port = Integer.parseInt(port);

        Response finalResponse = getResponseWithRouterPathAndCode(code, routePath);
        String responseString = finalResponse.asString();
        System.out.println(responseString);

        File responseFile = new File("src/test/response/response_"+date+".csv");
        FileWriter responseFileWriter = new FileWriter(responseFile);
        String responseBody = finalResponse.getBody().asString();

        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(responseBody);
        Set keySet = json.keySet();
        for(Object a : keySet){
            header = header + a + ",";
            body = body + json.get(a) + ",";
            System.out.println(a + "------"+json.get(a));
        }
        String totalResponse = header + "\n" + body;
        logger.info("Header: "+header+"\n"+"Body: "+body);
        responseFileWriter.write(totalResponse);
        responseFileWriter.close();

    }

    @Test(dataProviderClass = InitializeTestData.class, dataProvider = "readFromFile")
    public void validateEntireResponse(String get, String baseurl, String port, String routepath,  String params, String response, String code) throws IOException, ParseException {

        //initialising the base uri,
        RestAssured.baseURI = baseurl;
        RestAssured.port = Integer.parseInt(port);

        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(new FileReader(response));

        System.out.println(obj.toString());

        String jsonstring = obj.toString();

        Response finalresponse =   when().
                get(routepath).
                then().
                contentType(ContentType.JSON).  // check that the content type return from the API is JSON
                extract().response();

        String responseString = finalresponse.asString();
        System.out.println(jsonstring);
        System.out.println(responseString);

        Boolean bool = responseString.equals(jsonstring);
        System.out.println(bool);
        Assert.assertTrue(bool);
    }


    @Test(dataProviderClass = InitializeTestData.class, dataProvider = "readFromFile")
    public void jsonKeyComparisonsValidateResponse(String get, String baseurl, String port, String routepath,  String params, String response, String code)
            throws IOException, ParseException {

        //initialising the base uri,
        RestAssured.baseURI = baseurl;
        RestAssured.port = Integer.parseInt(port);

        //response from the get call
        Response getresponse =   when().
                get(routepath).
                then().
                contentType(ContentType.JSON).  // check that the content type return from the API is JSON
                extract().response();

        //convert response into json object
        String responseString = getresponse.asString();
        JSONParser jsonParser = new JSONParser();
        JSONObject responseJson  = (JSONObject) jsonParser.parse(responseString);

        //expected response (string)
        JSONParser parser = new JSONParser();
        JSONObject expectedJson = (JSONObject) parser.parse(new FileReader(response));

        if (expectedJson != null) {
            boolean result = validateKeysSet(responseJson, expectedJson.keySet());
            logger.info("Result received for validating Keys Sets " + result);
        }

    }


    private boolean validateKeysSet(JSONObject expectedObject, Set<String> receivedKeys) {
        if (receivedKeys == null)  return false;
        if (receivedKeys.size() != expectedObject.keySet().size()) return false;

        for (String key: receivedKeys) {
            if (!expectedObject.containsKey(key)) {
                logger.info("Received Json Response doesn't have key " + key);
                return false;
            }

        }

        return true;
    }

    private Response getResponseWithRouterPathAndCode( String code, String routePath) {
        return expect()
                .statusCode(Integer.parseInt(code))
                .when()
                .get(routePath);
    }


}