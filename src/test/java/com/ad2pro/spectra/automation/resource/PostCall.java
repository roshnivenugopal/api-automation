package com.ad2pro.spectra.automation.resource;


import com.ad2pro.spectra.automation.utils.InitializeTestData;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import static io.restassured.RestAssured.given;

public class PostCall {

    @Test(dataProviderClass = InitializeTestData.class, dataProvider = "readFromFile")
    public void simplePostCall(String get, String baseurl, String port, String routepath,  String params, String response, String code) throws IOException, ParseException {

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(response));
        String APIBody = obj.toString();

        // Building request using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Setting API's body
        builder.setBody(APIBody);

        //Setting content type as application/json or application/xml
        builder.setContentType("application/json; charset=UTF-8");
        RequestSpecification requestSpec = builder.build();

        //Making post request with authentication, leave blank in case there are no credentials- basic("","")
        Response getresponse = given().spec(requestSpec).when().post(routepath);
        JSONParser jsonParser = new JSONParser();
        JSONObject JSONResponseBody = (JSONObject) jsonParser.parse(getresponse.body().asString());

        //Fetching the desired value of a parameter
        Boolean hasornot = JSONResponseBody.containsKey("assetType");

        //Asserting that result of Norway is Oslo
        System.out.println("Has or not: "+hasornot);
        System.out.println("Status code: "+ getresponse.statusCode());
    }

    //parsing the keys and values from a json object
    @Test
    public void example() throws IOException, ParseException {

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader("src/test/input/json_input/rosh.json"));

        JSONObject jsonObject = (JSONObject) obj;
        System.out.println(jsonObject.toString());
        String name = (String) jsonObject.get("mediaType");
        System.out.println("Name: "+name);
        Set s = jsonObject.keySet();
        Collection val = jsonObject.values();

        for(Object a : s){
            System.out.println(a);
        }
        System.out.println(val.size());
        System.out.println(val);
    }


    @Test(dataProviderClass = InitializeTestData.class, dataProvider = "readFromFile")
    public void passInputFromFile(String get, String baseurl, String port, String routepath,  String params, String response, String code) throws IOException, ParseException {
        RestAssured.baseURI = baseurl;
        RestAssured.port = Integer.parseInt(port);

        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(new FileReader(response));
        String APIUrl = routepath;

        // Building request using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Setting API's body
        builder.setBody(obj.toString());

        //Setting content type as application/json or application/xml
        builder.setContentType("application/json; charset=UTF-8");

        RequestSpecification requestSpec = builder.build();

        Response postresponse = given().spec(requestSpec).when().post(APIUrl);
        System.out.println(postresponse.statusCode());
        System.out.println(obj.toString());
    }
}
