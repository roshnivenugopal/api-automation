package com.ad2pro.spectra.automation.utils;

import org.testng.annotations.DataProvider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InitializeTestData {

    private static final  String FILENAME = "src/test/resources/file.csv";;

    @DataProvider(name = "readFromFile")
    public Object[][] readCSV() throws IOException {
        List<List<String>> data = new ArrayList<>();
        BufferedReader fileData = new BufferedReader(new FileReader(FILENAME));
        String read;
        /*int rownum=0, colnum, row=0;
        String eachrow[] = null;*/

        //counting number of rows and columns
        while((read = fileData.readLine()) != null) {
            String[] rowData = read.split(",");
            List<String> tempRowData = new ArrayList<>();
            for(int i=0; i<rowData.length; i++){
                tempRowData.add(rowData[i].trim());
            }
            data.add(tempRowData);
        }
        int rowLength = data.size();
        int columnLength = 0;
        if (rowLength>0)
         columnLength = data.get(0).size();
        //taking values from csv
        Object[][] resultData = new Object[rowLength][columnLength];

        for (int i = 0; i < rowLength; i++) {
            resultData[i] = data.toArray();
        }

        /*while((readData = bufferedReader2.readLine()) != null){
            String[] rowdata = readData.split(",");
            for(int i=0; i<rowdata.length; i++){
                data[row][i] = (rowdata[i]).trim();
            }
            row++;
        }*/

        //returning the read object
        return resultData;

    }



}
